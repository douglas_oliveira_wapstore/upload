<?php 

require __DIR__.'./vendor/autoload.php';

use App\Files\Upload;
use App\Entity\File;


    //VERIFICA OS DADOS PASSADOS VIA POST
    if(isset($_POST['enviar'], $_FILES['files'])) {

        //SALVANDO O ARQUIVO NA PASTA ESPECÍFICA
        $obUpload = new Upload($_FILES['files']);

        //OBTÉM TODOS OS NOMES VÁLIDOS PARA SALVÁ-LOS NO BANCO DE DADOS
        $names = $obUpload->upload(__DIR__. DIRECTORY_SEPARATOR . 'img');


        //SALVANDO NO BANCO DE DADOS
        $obFile = new File($names);
        $obFile->cadastrar();
        
        header("Location: index.php?status=success");
        exit;
    
    }

    //OBTÉM TODOS OS ARQUIVOS DO BANCO
    $files = File::getFiles();



include __DIR__.'./includes/main.php';


?>