<?php

namespace App\Entity;
use App\Db\Database;
use PDO;

class File {

    /**
     * Identificador do arquivo
     * @var integer
     * 
     */
    public $id;

    /**
     * Nome do arquivo
     * @var string
     */
    public $nome;

    /**
     * Quantidade de downloads do arquivo
     * @var integer
     */
    public $downloads;

    /**
     * Construtor da classe
     */
    public function __construct($files = null) {

        if(!is_null($files)) {
            //CAPTURANDO INFORMAÇÕES DO ARQUIVO ENVIADO
            $this->nome = is_array($files) ? $files : array($files);
            $this->downloads = 0;
        }
    }


    /**
     * Função responsável por inserir arquivos no banco de dados
     * @return bool
     */
    public function cadastrar() {

        
            //CONECTA COM A TABELA DO BANCO
            $obDatabase = new Database('arquivo');
 
            foreach($this->nome as $file) {
                //INSERE OS DADOS DA TABELA
                $obDatabase->insert([
                    'nome' => $file,
                    'downloads' => $this->downloads
                ]);
            }

            //RETORNA SUCESSO
            return true;
    }

    /**
     * Função responsável por obter todos os arquivos cadastrados
     * @param string $where
     * @param string $order
     * @param string $limit
     * @param string $fields
     * @return PDO
     */
    static public function getFiles($where  = null, $order = null, $limit = null, $fields = '*') {
        return (new Database('arquivo'))->select($where, $order, $limit, $fields)->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    /**
     * Função responsável por retonar um arquivo específico do banco
     * @param integer $id
     * @return PDO
     */
    static public function getFile($id) {
        return (new Database('arquivo'))->select('id ='. $id)->fetchObject(self::class);
    }

    /**
     * Função responsável por alterar a informação correpsondente a determinado arquivo
     * @param array $value
     * @param string $where
     * @return boolean
     */

     public function alterar($id) {
         //ESTABELECE CONEXÃO COM O BANCO DE DADOS
         $obDatabase = new Database('arquivo');

         //VALORES A SEREM ALTERADOS
         $obDatabase->update('id = '. $id, [
             'nome' => $this->nome,
             'downloads' => $this->downloads
         ]);

         //RETORNA SUCESSO
         return true;
     }

}





?>