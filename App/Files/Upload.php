<?php 

namespace App\Files;

class Upload {


    /**
     * Nome do arquivo
     * @var string
     */
    private $name;

    /**
     * Tipo do arquivo
     * @var string
     */
    private $type;

    /**
     * Extensão do arquivo
     * @var string
     */
    private $extension;

    /**
     * Nome temporário do arquivo
     * @var string
     */
    private $tmp_name;

    /**
     * Tamanho do arquivo
     * @var string
     */
    private $size;

    /**
     * Variável de erro
     * @var string
     */
    private $error;

    /**
     * Variável que abriga a quantidade de duplicações do mesmo arquivo na pasta
     * @var integer
     */
    private $duplicates = [];

    /**
     * Construtor da classe
     * @param array $file
     */
    public function __construct($file) {
        $this->name = $file['name'];
        $this->type = $file['type'];
        $this->tmp_name = $file['tmp_name'];
        $this->error = $file['error'];
        $this->size = $file['size'];
        $this->extension = [];

        //PARA CADA ARQUIVO INSERIDO, SERÁ DADO SUA RESPECTIVA EXTENSÃO E DUPLICATE NULO
        foreach($file['name'] as $file) {
            $this->extension[] = pathinfo($file)['extension'];
            $this->duplicates[] = null;
        }

    }

    /**
     * Função responsável por obter um nome que será testado para um arquivo
     * @param integer $offset
     * @return string
     */
    private function getBasename($offset) {

        //VERIFICA SE O VALOR DA DUPLICAÇÃO É NULO
        if($this->duplicates[$offset] === null) {
            return $this->name[$offset];
        }

        //EXTRAI O NOME DO ARQUIVO
        $name = basename($this->name[$offset], (isset($this->extension[$offset]) ? '.'.$this->extension[$offset] : ''));

        //RETORNA UM NOME POSSÍVEL
        return $name . '-' . $this->duplicates[$offset] . (strlen($this->extension[$offset]) ? '.' . $this->extension[$offset] : '');
        
    }

    /**
     * Função responsável por obter um nome possível para um arquivo
     * @param string $dir
     * @param integer $overwrite
     * @param integer $offset
     * @return string
     */

    private function getPossibleName($dir, $offset, $overwrite = 0) {

        //VERIFICA SE É PERMITIDA A SOBRESCRITA
        if ($overwrite == 1) return basename($this->name[$offset]);

        //OBTÉM UM NOME PARA O QUAL SERÁ VERICADO EXISTÊNCIA
         $basename = $this->getBasename($offset);

        //VERIFICA SE O ARQUIVO JÁ EXISTE NA PASTA INFORMADA
        if(!file_exists($dir . DIRECTORY_SEPARATOR. $basename)) {
            return basename($basename);
        }

        //INCREMENTA A VARIÁVEL DE DUPLICAÇÃO
        $this->duplicates[$offset]++;

        return $this->getPossibleName($dir, $offset, $overwrite);

    }


    /**
     * Função responsável por realizar o upload de arquivos no sistema
     * @param string $dir
     * @param integer $overwrite
     */
    public function upload($dir, $overwrite = 0) {

            //array contendo todos os nomes corretos
            $files = [];
     
              //PARA CADA ARQUIVO INSERIDO, SERÁ MONTADO UM ARRAY 
              for($x = 0; $x <= (sizeof($this->name) -1); $x++) {                  
                     
             //OBTÉM UM NOME POSSÍVEL PARA O ARQUIVO
             $fileName = $this->getPossibleName($dir,$x,$overwrite);

             $files[] = $fileName;

              
             //MOVE O ARQUIVO PARA UMA PASTA ESPECÍFICA
             move_uploaded_file($this->tmp_name[$x], $dir . DIRECTORY_SEPARATOR . $fileName);

                }
            

        //RETORNA SUCESSO
        return $files;

    }





}




?>