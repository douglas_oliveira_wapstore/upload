<?php 

require __DIR__.'./vendor/autoload.php';

use App\Files\Upload;
use App\Entity\File;

//VERIFICAÇÕES NA URL
if(!isset($_GET['id'])||!is_numeric($_GET['id'])) {
    header("Location: index.php?status=error");
    exit;
}

//OBJETO DO ARQUIVO
$obFile = File::getFile($_GET['id']);
$obFile->downloads++;
$obFile->alterar($_GET['id']);


header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename($obFile->nome));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize('img/'.$obFile->nome));
readfile('img/'.basename($obFile->nome));





include __DIR__.'./includes/main.php';


?>