<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Uplaod de arquivos</title>
  </head>


  <?php 
  
  $msgStatus = '';
  
  if(isset($_GET['status'])) {
    switch ($_GET['status']) {
      case 'success':
        $msgStatus = "<div class='alert alert-success'>Arquivos adicionados com sucesso</div>";
        break;

      case 'error':
        $msgStatus = "<div class='alert alert-danger'>Houve um erro.</div>";
        break;

    }
  }

  $listagemFiles = '';


  foreach($files as $file) {

    $listagemFiles .= '<tr>
                          <td>'. $file->id . '</td>
                          <td>'. pathinfo($file->nome)['filename'] . '</td>
                          <td>'. pathinfo($file->nome)['extension'] . '</td>
                          <td>'. $file->downloads . '</td>
                          <td><a href="download.php?id='.$file->id.'"><button type="button" class="btn btn-outline-primary"> Download </button></a>
                      
                      </tr>    ';
  }
  
  
  
  ?>



  <body>

    <header class="container jumbotron bg-light p-4">
        <h1 class="display-4">Upload de arquivos</h1>
        <br>
        <hr class="bg-dark">
        <br>
        <p class="lead">Tratando e aprendendo upload de arquivos</p>

    </header>


    <main class="container my-4">
      <?=$msgStatus?>

        <form method="post" class="form" enctype="multipart/form-data">
        
            <div class="form-group my-4">

                <input type="file" name="files[]" class="form-control" required multiple>

            </div>

            <div class="form-group">

                <button class="btn btn-outline-dark" name="enviar">Enviar</button>

            </div>


          <div class="container my-4">
          
            <table class="table">
            
              <thead>

                <th>ID</th>
                <th>Nome</th>
                <th>Extensão</th>
                <th>Downloads</th>
                <th>Ações</th>

              </thead>

              <tbody>
              
                  <?=$listagemFiles?>
              
              </tbody>
            
            </table>
          
          </div>
        
        </form>


    </main>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

  </body>
</html>